<?php

namespace Drupal\blog_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\UncacheableDependencyTrait;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "user_profile_block",
 *   admin_label = @Translation("User Profile Block"),
 * )
 */
class UserProfileBlock extends BlockBase {

    // Nice stuff!
    use UncacheableDependencyTrait;

    /**
     * Build Function.
     */
    public function build() {

        $current_user = \Drupal::currentUser(); 
        $user_mail = $current_user->getEmail();

        return [
            '#markup' => $this->t($user_mail),
        ];
    }

}