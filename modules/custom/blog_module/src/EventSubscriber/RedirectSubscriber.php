<?php

namespace Drupal\blog_module\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RedirectSubscriber implements EventSubscriberInterface {

    public static function checkForRedirection(RequestEvent $event) {
        $current_path = \Drupal::service('path.current')->getPath();
        if ($current_path == '/users') {
            $response = new RedirectResponse(\Drupal\Core\Url::fromRoute('<front>')->toString());
            $response->send();
        }
    }


    public static function getSubscribedEvents() {
        return [
          KernelEvents::REQUEST => 'checkForRedirection',
        ];
      }
    
}